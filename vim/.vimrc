" ~/.vimrc

syntax on
filetype plugin indent on

set nocompatible
set encoding=utf-8
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu rnu
set cursorline
set scrolloff=5
set wrap
set incsearch
set smartcase
set mouse=n
set noswapfile nobackup
set colorcolumn=80
set showcmd
set list lcs=tab:\┊\

" Plugins (vim-plug)
call plug#begin('~/.vim/plugged')

Plug 'aonemd/kuroi.vim'
Plug 'ycm-core/YouCompleteMe'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-fugitive'
Plug 'vim-utils/vim-man'
Plug 'vim-syntastic/syntastic'
Plug 'lotabout/skim.vim'

Plug 'rust-lang/rust.vim'

call plug#end()

" Plugins configs
let python_space_error_highlight = 1

set background=dark
set termguicolors
colorscheme kuroi

let g:ycm_autoclose_preview_window_after_completion=1

let g:loaded_syntastic_java_javac_checker = 1

if executable('rg')
	let g:rg_derive_root='true'
endif

let g:netrw_browse_split = 2
let g:netrw_banner = 0
let g:netrw_winsize = 25

" Mapping
let mapleader = " "

nnoremap <C-h> :wincmd h<CR>
nnoremap <C-j> :wincmd j<CR>
nnoremap <C-k> :wincmd k<CR>
nnoremap <C-l> :wincmd l<CR>

inoremap (( ()<Left>
inoremap [[ []<Left>
inoremap {{ {}<Left>
inoremap << <><Left>
inoremap "" ""<Left>
inoremap '' ''<Left>

inoremap ;; <Esc>A;

vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

nnoremap <leader>n :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>

nnoremap <leader>/ :Rg<Space>

nnoremap <silent> <leader>gd :YcmCompleter GoTo<CR>
nnoremap <silent> <leader>gf :YcmCompleter FixIt<CR>

nnoremap <leader>f :SK<CR>
