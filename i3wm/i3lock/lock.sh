#! /usr/bin/sh

WHITE='#c4c4c4ff'
BLACK='#4c4c4c88'
CLEAR='#00000000'

i3lock \
\
--screen 1 \
--blur 5 \
--clock \
--indicator \
\
--insidecolor=$BLACK \
--ringcolor=$CLEAR \
--linecolor=$WHITE \
--separatorcolor=$CLEAR \
\
--verifcolor=$WHITE \
--insidevercolor=$BLACK \
--ringvercolor=$CLEAR \
\
--wrongcolor=$WHITE \
--insidewrongcolor=$BLACK \
--ringwrongcolor=$BLACK \
\
--timecolor=$WHITE \
--datecolor=$WHITE \
--keyhlcolor=$WHITE \
--bshlcolor=$WHITE \
\
--timestr="%H:%M" \
--datestr="%d-%m-%Y" \
--veriftext="..." \
--wrongtext="Fail!" \
\
--time-font="Hack" \
--date-font="Hack" \
--verif-font="Hack" \
--wrong-font="Hack" \
