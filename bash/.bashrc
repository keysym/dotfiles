#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ip='ip -c'

PS1='\u@\h \e[0;1m\W\e[0m \$ '
if [ "$SSH_CONNECTION" ]; then
	PS1='\e[1;36mssh\e[0m:'$PS1
fi
if [ $VIM ]; then
	PS1='\e[0;32mvim\e[0m:'$PS1
fi

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
	. /usr/share/bash-completion/bash_completion

